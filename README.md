# Juri Gabori

Wie es begann ...

![](img/juri_banner_mix.jpeg)
## [Erster Brief](./letters/01.md) 

----

### 2013/04/13
Wir werden ein paar von Juri Gaboris Songs am Samstag, den 25. Mai in unserem
Proberaum in Seiersberg zum Besten geben.

Otto-Baumgartner-Straße 10-11, 8055 Seiersberg

Start 20.00 Uhr

----

### 2014/07/08
Auch in diesem Jahr spielen wir wieder einige Songs von Juri Gabori. Wir (Bo,
Ricky, Sir P., Gallagher) finden uns am Samstag, den 02. August in unserem
Proberaum zusammen und präsentieren euch Lost Songs.

Otto-Baumgartner-Straße 10-11, 8055 Seiersberg

Start 21.00 Uhr (pünktlich)

Schon vorab Danke an Didi und Jakob, die uns eine kurze musikalische Einstimmung
für diesen Abend geben.

----

### 2014/08/01
Nur noch eine Nacht und wir stehen vor unser zweiten Präsentation einiger Juri
Gabori Lieder. Es ist auch wieder ein Brief mit einem Bild aufgetaucht, aus dem
wir wieder etwas mehr aus dem Leben von Juri erfahren. Er wird langsam fast zu
einem Teil unserer Familie.

… Und hier sind wir nun.

Eigentlich dachte ich mir nichts dabei, als ich vor 7 Jahren beim
Ausräumen des Hauses meiner verstorbenen Großmutter in einer Kommode
dieses Päckchen fand. Ein dicker Briefumschlag mit Bildern eines mir
unbekannten Mannes. Dabei war auch ein Brief, der wirkte wie der Teil
einer umfangreicheren Korrespondenz. Mein Interesse war von kurzer
Dauer; das Päckchen behielt ich aber und verstaute es in meiner eigenen
‘Kommode’ – dem Platz für Dinge, die man nicht braucht, von denen man
sich aber nicht trennen will.

Vor einem Jahr erinnerte ich mich wieder daran, kramte es hervor und
entdeckte bei näherer Inspektion die Adresse eines Notars in Berlin (nicht
das in Deutschland – New Hampshire) zusammen mit der Anweisung, meine
Großmutter möge doch mit diesem Herrn Kontakt aufnehmen.

Mein Interesse war geweckt, ein Brief, (irgendwie hatte mir diese ganze
Sache wieder Lust auf Papier gemacht – und was sollten schon diese paar
zusätzlichen Wochen ausmachen) in dem ich mich erklärte, war schnell
aufgesetzt und verschickt. Die Antwort kam prompt : Ein weiteres
Päckchen – Inhalt (unter anderem): Notenblätter!!

Ein kurzes Schreiben war beigefügt: Der Notar erklärte, den Auftrag
erhalten zu haben, den Nachlass seines Mandanten – Juri Gabori – meiner
Großmutter – oder wie er sich ausdrückte – in diesem Fall ihrem rechtlichen
Nachfolger auszuliefern. Mehr würde folgen.

![](img/juri_banner_eyes_young.jpeg)
## [Zweiter Brief](./letters/02.md)

----

